const express = require('express');
const { GoogleAuth } = require('google-auth-library')
const {google} = require('googleapis');
const axios = require('axios');
const app = express();

const KEYFILEPATH = `./credentials.json`;
const SCOPES = ['https://www.googleapis.com/auth/drive'];

app.get('/google-drive-file', async (req, res, next) => {
  const files = [];
  try {
    const auth = new GoogleAuth({
      keyFile: KEYFILEPATH,
      scopes: SCOPES
    });
    

  
    const result = await google.drive({
      version: 'v2',
      auth
    }).files.get({
      fileId: '1_K_ldUuuUpC1ua6QuQPOZcP5tAjtDTIL',
      spaces: 'drive',
    });
  
    Array.prototype.push.apply(files, result.files);
  
    const file = await axios({
      method: 'get',
      url: result.data.webContentLink
    });
  
    res.send(file.data);
    
  } catch (error) {
    next(new Error('Probably you forgot to populate credentials.json with data from the email i sent'));
  }
  
});

app.use((error, req, res, next) => {
  res.status(500).json({
    error: error.message
  })
})

app.listen(3000, async () => {
  console.log('Server listening at port 3000');
});