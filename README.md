# FrontendAssignment
To run this project, please simply copy and paste the credentials i sent over email to credentials.json file in the root of the project. Without it, the project won't work.

## Running the project
After add the credentials file, please run `npm install` and then `npm start` and on another terminal run the server using `npm run server`

## Implementation notes
* Due to the difficulty of using google drive api from the frontend, i created a server to handle fetching of the data from google drive.
* I then had to parse the CSV to JSON on the client so that it can be displayed.
* I created a mini-version of a datatable with pagination so as not to weigh the browser down with the rendering of over 36000 rows of data.
* I added the Custom Column at the level of the parsing of the CSV to JSON because i felt it made more sense to add it over there.