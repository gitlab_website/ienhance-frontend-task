import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { catchError, of } from 'rxjs';
import { GoogleDriveService } from './services/google-drive.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  title = 'frontend-assignment';
  jsonData: any[] = []
  tableHeaders: any[] = []
  error: string = '';

  constructor(private googleDriveServ: GoogleDriveService) {}

  async ngOnInit() {
    this.googleDriveServ.getCsvFile()
      .subscribe(async (result) => {
        if (typeof result === 'string') {
          this.jsonData = await this.convToJSONWithCustomColumn(result);
          this.tableHeaders = this.getTableHeaders(this.jsonData);
        } else {
          this.error = result.message;
        }
      });
  }
  async convToJSONWithCustomColumn(csvFileString: string) {
    const csvHeaders = [];
    const jsonResult: any[] = []
    const rows = csvFileString.split('\r');
    
    for (let i = 0; i < rows.length; i++) {
      const cells = rows[i].split(',');
      const rowData: any = {};

      for (let j = 0; j < cells.length; j++) {
        if (i === 0) { // Means that we are at the header of the csv data
          const headerName = cells[j].trim();
          csvHeaders.push(headerName);

          // When we are at the ending of the header row, compute the custom column
          if(cells.length - j === 1) { 
            csvHeaders.push('Custom');
          }
        } else {  // Means that we are at the body of the csv data
          const key = csvHeaders[j];
          rowData[key] = cells[j].trim();

          // When we are at the ending of each of the body rows, compute the custom
          // column
          if(cells.length -j === 1) {
            const key = csvHeaders[j+1];
            rowData[key] = cells[j-1].trim() + cells[j].trim();
          }
        }
      }

      // Do not push the header row into the json array object
      if (i != 0) {
        jsonResult.push(rowData);
      }
    }

    return jsonResult;
  }

  getTableHeaders(jsonData: any[]) {
    return Object.keys(jsonData[0])
  }
}
