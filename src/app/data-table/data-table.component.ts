import { Component, Input, OnInit } from "@angular/core";
import { Data } from "../models/data.interface";
import { FormControl } from "@angular/forms";

@Component({
  selector: 'app-data-table',
  templateUrl: './data-table.component.html',
  styleUrls: ['./data-table.component.scss']
})
export class DataTableComponent implements OnInit {
  @Input() data: Data[] = [];
  @Input() qtySet = [50, 100, 200];
  totalItems: number = 0;
  currentPage = 0;
  displayedData: Data[] = [];
  pageStats = '';

  qtySelector = new FormControl();

  get columnHeadings() {
    return Object.keys(this.data?.[0]);
  }

  getColumnValue(obj: Data) {
    return Object.values(obj); 
  }

  getLastPage() {
    const numberOfFullPages = this.data.length / this.itemsPerPage;
    const hasRemnant = this.data.length % this.itemsPerPage;
    if(hasRemnant) {
      return Math.floor(numberOfFullPages) + 1;
    }
    return numberOfFullPages;
  }

  ngOnInit(): void {
    this.qtySelector.setValue(this.qtySet[0]);
    this.totalItems = this.data.length;
    this.getDisplayedData();
    this.qtySelector.valueChanges.subscribe(() => {
      this.getDisplayedData()
    })
  }

  get itemsPerPage() {
    return +this.qtySelector.value;
  }

  getPageStats(): string{
    if(this.currentPage === 0) {
      return `1 - ${this.itemsPerPage} of ${this.data.length}`
    } else if(this.currentPage === this.getLastPage() - 1){
      return `${(this.itemsPerPage * this.currentPage) + 1} - ${(this.data.length)} of ${this.data.length}`
    }
    else{
      return `${(this.itemsPerPage * this.currentPage) + 1} - ${(this.itemsPerPage * this.currentPage) + this.itemsPerPage} of ${this.data.length}`
    }
  }

  getDisplayedData(): void {
    if(this.currentPage === 0) {
      this.displayedData = this.data.slice(0, this.itemsPerPage);
    } else {
      const nextItemStart = this.data.slice(this.itemsPerPage * this.currentPage, this.itemsPerPage * (this.currentPage + 1))
      this.displayedData =  nextItemStart;
    }
    this.pageStats = this.getPageStats();
  }
  goToNextPage() {
    if(this.currentPage < this.getLastPage() -1) {
      this.currentPage++;
      this.getDisplayedData();
    }
  }
  goToPrevPage() {
    if(this.currentPage !== 0) {
      this.currentPage--;
      this.getDisplayedData();
    }
  }
  goToFirstPage() {
    this.currentPage = 0;
    this.getDisplayedData();
  }

  goToLastPage() {
    this.currentPage = this.getLastPage() - 1;
    this.getDisplayedData();
  }
}