import { HttpClient, HttpErrorResponse, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { catchError, of } from "rxjs";

@Injectable({
  providedIn: 'root',
})
export class GoogleDriveService {
  constructor(private httpClient: HttpClient) {}

  getCsvFile() {
    return this.httpClient
      .get('/google-drive-file', {
        headers: new HttpHeaders().append('Content-Type', 'text'),
        responseType: 'text',
      })
      .pipe(
        catchError((httpError: HttpErrorResponse) => {
          const responseError = JSON.parse(httpError.error);
          return of(new Error(responseError.error));
        })
      );
  }
}